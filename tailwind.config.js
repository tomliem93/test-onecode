// eslint-disable-next-line @typescript-eslint/no-var-requires
const { colors, fontFamily } = require('tailwindcss/defaultTheme')

module.exports = {
  content: ['./src/**/*.{ts,tsx}'],
  theme: {
    colors: {
      primary: '#604412',
    },
    extend: {
      colors,
      fontFamily: {
        primary: [...fontFamily.sans],
        sans: [...fontFamily.sans],
      },
      container: {
        center: true,
        padding: {
          DEFAULT: '0.75rem',
          sm: '1rem',
        },
        screens: {
          xl: '1140px',
          '2xl': '1140px',
        },
      },
      boxShadow: {
        homeBox: '0px 32px 40px -30px rgba(180, 130, 41, 0.6)',
        appInput: '0 0 4px 0 rgba(96,68,18,0.50)',
        sidebar: '0 0 4px 2px rgba(0,0,0,0.10)',
      },
      dropShadow: {
        homeIcon: '0px 0px 30px rgba(180, 130, 41, 0.5)',
      },
    },
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/aspect-ratio')],
}
