const nextConfig = {
  images: {
    domains: ['storage.googleapis.com', 'dragondev.s3.ap-southeast-1.amazonaws.com'],
  },
  reactStrictMode: true,
  swcMinify: true,
  output: 'standalone',
  eslint: {
    dirs: ['src'],
  },
  pwa: {
    dest: 'public',
    disable: process.env.NODE_ENV === 'development',
  },
  publicRuntimeConfig: {
    siteName: process.env.SITE_NAME,
    siteUrl: process.env.SITE_URL,
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/i,
      issuer: /\.[jt]sx?$/,
      use: ['@svgr/webpack'],
    })

    return config
  },
}

module.exports = nextConfig
