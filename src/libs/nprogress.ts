import NProgress from 'nprogress'

export const startProgress = () => {
  NProgress.start()
}

export const stopProgress = () => {
  NProgress.done()
}
