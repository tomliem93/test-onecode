import NextAuth, { NextAuthOptions, Session, User } from 'next-auth'
import { JWT } from 'next-auth/jwt'
import CredentialsProvider from 'next-auth/providers/credentials'

import api from '@/services/api'

const providers = [
  CredentialsProvider({
    name: 'Credentials',
    credentials: {
      username: { label: 'Username', type: 'text', placeholder: 'Username' },
      password: { label: 'Password', type: 'password', placeholder: 'Password' },
    },
    authorize: async (credentials) => {
      const user = await api.auth.submitLogin({
        username: credentials?.username ?? '',
        password: credentials?.password ?? '',
      })
      if (user) {
        return {
          name: user.name,
          username: user.username,
          email: user.email,
          address: `${user.address.suite}. ${user.address.city}, ${user.address.street} ${user.address.zipcode}`,
          phone: user.phone,
        }
      }

      throw new Error('Incorrect username or password')
    },
  }),
]

const callbacks = {
  async jwt({ token, user }: { token: JWT; user?: User }) {
    if (user) {
      token.username = user.username
      token.name = user.name
      token.email = user.email

      token.address = user.address
      token.phone = user.phone
    }

    return token
  },

  async session({ session, token }: { session: Session; token: JWT }) {
    if (token) {
      session.username = token.username
      session.name = token.name
      session.email = token.email
      session.address = token.address
      session.phone = token.phone
    }
    if (session.user) {
      session.user.image = null
    }

    return session
  },
}

const pages = {
  signIn: '/auth/signin',
  error: '/auth/signin',
}
export const authOptions: NextAuthOptions = {
  providers,
  callbacks,
  pages,
  debug: process.env.NODE_ENV === 'development',
  jwt: {
    secret: process.env.NEXTAUTH_SECRET_KEY || 'secret',
  },
}

export default NextAuth(authOptions)
