import { Popover, Transition } from '@headlessui/react'
import { Icon } from '@iconify/react'
import { useQuery } from '@tanstack/react-query'
import type { NextPage } from 'next'
import { GetServerSideProps } from 'next'
import { useRouter } from 'next/router'
import { unstable_getServerSession } from 'next-auth'
import { Fragment } from 'react'

import Layout from '@/components/base/Layout'
import api from '@/services/api'

import { authOptions } from '../../api/auth/[...nextauth]'

export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  const session = await unstable_getServerSession(req, res, authOptions)
  if (!session) {
    return {
      redirect: {
        destination: '/auth/signin',
        permanent: false,
      },
    }
  }
  return {
    props: {
      session,
    },
  }
}
const App: NextPage = () => {
  const router = useRouter()
  const { id } = router.query

  const { data } = useQuery(['PostDetail', id], () => api.master.getPostDetail(Number(id)), { enabled: !!id })

  const { data: commentData } = useQuery(['PostComment', id], () => api.master.getComments(Number(id)), {
    enabled: !!id,
  })
  console.log(commentData)
  return (
    <Layout>
      <div className="flex justify-start w-full">
        <button onClick={() => router.back()}>
          <Icon className="w-10 h-10" icon="eva:arrow-back-outline" />
        </button>
      </div>
      <div className="flex flex-col">
        <div className="flex flex-row space-x-4">
          <div className="w-20 mr-2"></div>
          <p className="text-slate-600">{data?.title}</p>
        </div>
        <div className="flex flex-row space-x-4">
          <div className="w-20 font-semibold">Username</div>
          <div className="flex flex-col relative">
            <p>{data?.body}</p>
            <Popover className={`relative`}>
              {({ open }) => (
                <>
                  <Popover.Button className="flex flex-row items-center justify-start cursor-pointer">
                    {open ? (
                      <span className="font-semibold mt-2">All Comments</span>
                    ) : (
                      <>
                        <Icon icon="bxs:comment-detail" /> <span>{commentData?.length || 0}</span>
                      </>
                    )}
                  </Popover.Button>
                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-200"
                    enterFrom="opacity-0 translate-y-1"
                    enterTo="opacity-100 translate-y-0"
                    leave="transition ease-in duration-150"
                    leaveFrom="opacity-100 translate-y-0"
                    leaveTo="opacity-0 translate-y-1"
                  >
                    <Popover.Panel className="absolute left-1/2 z-10 mt-3 w-full -translate-x-1/2 transform px-4 sm:px-0 lg:max-w-3xl">
                      <div className="overflow-hidden rounded-lg">
                        <div className="relative flex flex-col bg-white p-4 space-y-2">
                          {(commentData || []).map((item) => (
                            <div
                              key={item.id}
                              className="flex rounded-lg p-2 transition duration-150 ease-in-out hover:bg-gray-50 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50"
                            >
                              <div className="flex h-full items-start justify-end text-gray-500 font-semibold w-20">
                                {item.email.slice(0, item.email.indexOf('@'))}
                              </div>
                              <div className="ml-4 flex flex-1">
                                <p className="text-sm text-gray-500">{item.body}</p>
                              </div>
                            </div>
                          ))}
                        </div>
                      </div>
                    </Popover.Panel>
                  </Transition>
                </>
              )}
            </Popover>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default App
