import { Icon } from '@iconify/react'
import { useQuery } from '@tanstack/react-query'
import clsx from 'clsx'
import type { NextPage } from 'next'
import { GetServerSideProps } from 'next'
import { unstable_getServerSession } from 'next-auth'
import { useState } from 'react'

import Layout from '@/components/base/Layout'
import { PostItem, PostItemSkeleton } from '@/components/posts/PostItem'
import useDebounce from '@/hooks/useDebounce'
import api from '@/services/api'

import { authOptions } from '../api/auth/[...nextauth]'
export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  const session = await unstable_getServerSession(req, res, authOptions)
  if (!session) {
    return {
      redirect: {
        destination: '/auth/signin',
        permanent: false,
      },
    }
  }
  return {
    props: {
      session,
    },
  }
}
const App: NextPage = () => {
  const [query, setQuery] = useState({
    search: '',
    page: 0,
    limit: 10,
  })
  const debouncedSearch = useDebounce(query.search, 500)
  const { data, isFetching } = useQuery(
    ['factorys', debouncedSearch, query.limit, query.page],
    () => api.master.getPosts(query),
    {
      onError: () => console.log('error'),
    }
  )

  const totalPage = Math.round((data?.total || 0) / query.limit)
  const renderPagination = () => {
    const paging = []

    for (let i = 0; i <= totalPage; i++) {
      paging.push(
        <a
          href="#"
          onClick={() => setQuery((prev) => ({ ...prev, page: i }))}
          className={clsx(
            'px-4 py-2  rounded-md hover:bg-blue-400 hover:text-white',
            i === query.page ? 'bg-blue-600 text-white' : 'bg-gray-200 text-gray-700 '
          )}
        >
          {i + 1}
        </a>
      )
    }
    return paging
  }
  return (
    <Layout>
      <section className="flex items-center flex-col">
        <h1 className="text-slate-500 border-b-blue-600 border-b-[3px] pb-2 w-fit">Posts</h1>

        <div className="my-8 relative flex flex-row overflow-hidden  rounded-full w-auto h-11 items-center">
          <input
            type="text"
            className="border-transparent focus:border-transparent focus:ring-0 z-10 ml-5 text-center"
            placeholder="Search..."
            onChange={(e) => setQuery((prev) => ({ ...prev, search: e.target.value, page: 0 }))}
            value={query.search}
          />
          <fieldset className="w-full h-full absolute border-slate-500 border bg-transparent rounded-full "></fieldset>
          <Icon icon="bx:search" className="w-6 h-6 mr-4" />
        </div>

        {isFetching ? (
          [1, 2, 3, 4].map((item) => <PostItemSkeleton key={item} />)
        ) : (
          <>
            <div className="flex flex-col">
              {(data?.data || []).map((item) => {
                return <PostItem key={item.id} postId={item.id} title={item.title} />
              })}
            </div>
            {data?.total && (
              <div className="flex items-center space-x-1 my-4">
                <a
                  onClick={() => setQuery((prev) => ({ ...prev, page: prev.page > 0 ? prev.page - 1 : 0 }))}
                  href="#"
                  className="flex items-center px-4 py-2 text-gray-500 bg-gray-300 rounded-md"
                >
                  Previous
                </a>
                {renderPagination()}
                <a
                  onClick={() =>
                    setQuery((prev) => ({ ...prev, page: prev.page < totalPage ? prev.page + 1 : totalPage }))
                  }
                  href="#"
                  className="px-4 py-2 font-bold text-gray-500 bg-gray-300 rounded-md hover:bg-blue-600 hover:text-white"
                >
                  Next
                </a>
              </div>
            )}
          </>
        )}
      </section>
    </Layout>
  )
}

export default App
