import { Icon } from '@iconify/react'
import type { NextPage } from 'next'
import { GetServerSideProps } from 'next'
import { useRouter } from 'next/router'
import { unstable_getServerSession } from 'next-auth'
import { useSession } from 'next-auth/react'

import Layout from '@/components/base/Layout'

import { authOptions } from '../api/auth/[...nextauth]'
export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  const session = await unstable_getServerSession(req, res, authOptions)
  if (!session) {
    return {
      redirect: {
        destination: '/auth/signin',
        permanent: false,
      },
    }
  }
  return {
    props: {
      session,
    },
  }
}
const App: NextPage = () => {
  const { data } = useSession()
  const router = useRouter()
  return (
    <Layout>
      <div className="flex justify-start w-full">
        <button onClick={() => router.back()}>
          <Icon className="w-10 h-10" icon="eva:arrow-back-outline" />
        </button>
      </div>
      <div className="flex flex-col">
        <div className="flex flex-row w-96">
          <div className="w-24 font-semibold">Username</div>
          <div className="w-5">:</div>
          <div className="flex-auto">{data?.username}</div>
        </div>

        <div className="flex flex-row w-96">
          <div className="w-24 font-semibold">Email</div>
          <div className="w-5">:</div>
          <div className="flex-auto">{data?.email}</div>
        </div>

        <div className="flex flex-row w-96">
          <div className="w-24 font-semibold">Address</div>
          <div className="w-5">:</div>
          <div className="flex-1">{data?.address}</div>
        </div>

        <div className="flex flex-row w-96">
          <div className="w-24 font-semibold">Phone</div>
          <div className="w-5">:</div>
          <div className="flex-auto">{data?.phone}</div>
        </div>
      </div>
    </Layout>
  )
}

export default App
