import type { NextPage } from 'next'

const Offline: NextPage = () => (
  <>
    <div className="h-screen bg-slate-100">
      <main className="flex h-screen items-center justify-center">
        <div className="text-center">
          <h1 className="text-2xl text-slate-300">You are offline</h1>
        </div>
      </main>
    </div>
  </>
)

export default Offline
