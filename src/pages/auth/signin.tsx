import { GetServerSideProps } from 'next'
import { unstable_getServerSession } from 'next-auth'

import LoginForm from '@/components/auth/LoginForm'
import Layout from '@/components/base/Layout'

import { authOptions } from '../api/auth/[...nextauth]'

const Login = () => {
  return (
    <Layout>
      <LoginForm />
    </Layout>
  )
}

export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  const session = await unstable_getServerSession(req, res, authOptions)

  if (session) {
    return {
      redirect: {
        destination: '/app',
        permanent: false,
      },
    }
  }

  return {
    props: {
      session,
    },
  }
}

export default Login
