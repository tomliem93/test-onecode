import Layout from '@/components/base/Layout'

const Custom404 = () => (
  <Layout>
    <section className="flex h-[768px] items-center justify-center bg-light pt-28">
      <h1 className="text-center text-lg text-primary">
        <span className="block w-full text-9xl font-bold text-secondary">404</span>Page Not Found
      </h1>
    </section>
  </Layout>
)

export default Custom404
