import type { NextPage } from 'next'

import Layout from '@/components/base/Layout'

const Home: NextPage = () => {
  return (
    <Layout>
      <section>Home Page</section>
    </Layout>
  )
}

export default Home
