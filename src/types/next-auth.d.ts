export {}

declare module 'next-auth' {
  interface User {
    username: string
    name: string
    email: string
    address: string
    phone: string
  }
  interface Session {
    username: string
    name: string
    email: string
    address: string
    phone: string
  }
}

declare module 'next-auth/jwt' {
  interface JWT {
    username: string
    name: string
    email: string
    address: string
    phone: string
  }
}
