import { Icon } from '@iconify/react'
import { useRouter } from 'next/router'
import { signIn } from 'next-auth/react'
import { useState } from 'react'
import { Controller, useForm } from 'react-hook-form'

import Alert from '@/components/common/Alert'
import { LoginReqData } from '@/services/api/auth'

const LoginForm = () => {
  const [isLoginStarted, setIsLoginStarted] = useState(false)
  const { query } = useRouter()
  const {
    control,
    formState: { errors, isDirty, isValid },
    handleSubmit,
  } = useForm<LoginReqData>({ mode: 'onChange' })

  const onSubmit = (data: LoginReqData) => {
    setIsLoginStarted(true)
    signIn('credentials', {
      username: data.username,
      password: data.password,
      callbackUrl: query.callbackUrl ? query.callbackUrl.toString() : `${window.location.origin}/app`,
    })
  }

  return (
    <form
      onSubmit={handleSubmit((data) =>
        onSubmit({
          username: data.username,
          password: data.password,
        })
      )}
      className="flex flex-col items-center"
    >
      {query.error && (
        <Alert variant="danger" floated>
          {query.error}
        </Alert>
      )}
      <h2 className="text-blue-500 font-semibold">Login</h2>
      <Controller
        name="username"
        control={control}
        rules={{
          required: { value: true, message: 'Username is required' },
          minLength: {
            value: 3,
            message: 'Username cannot be less than 3 character',
          },
        }}
        render={({ field: { value, onChange } }) => (
          <input
            className="w-64 h-11 border-gray-400 block rounded-lg mb-2"
            type="text"
            placeholder="Username"
            value={value || ''}
            onChange={onChange}
          />
        )}
      />
      <div>{errors.username?.message}</div>
      <Controller
        name="password"
        control={control}
        rules={{
          required: { value: true, message: 'Password is required' },
        }}
        render={({ field: { value, onChange } }) => (
          <input
            className="w-64 h-11 border-gray-400 block rounded-lg mb-2"
            type="password"
            placeholder="Password"
            value={value || ''}
            onChange={onChange}
          />
        )}
      />
      <div>{errors.password?.message}</div>
      <hr className="mb-5 border-app-light" />
      <button
        className="text-white cursor-pointer w-full bg-gradient-to-br from-green-400 to-blue-600 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-green-200 dark:focus:ring-green-800 font-medium rounded-full text-sm px-5 py-2.5 text-center"
        type="submit"
        disabled={!isDirty || !isValid || isLoginStarted}
      >
        {isLoginStarted ? (
          <>
            <Icon icon="gg:spinner" className="mr-2 inline-block animate-spin" /> Mohon tunggu
          </>
        ) : (
          'Masuk'
        )}
      </button>
    </form>
  )
}

export default LoginForm
