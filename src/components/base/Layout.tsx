import Header from './Header'

interface LayoutProps {
  children: React.ReactNode
}

const Layout = ({ children }: LayoutProps) => {
  return (
    <>
      <Header />
      <main className="pt-20 flex flex-col items-center container lg:max-w-xl">{children}</main>
    </>
  )
}

export default Layout
