import { Menu, Transition } from '@headlessui/react'
import cx from 'clsx'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { signOut, useSession } from 'next-auth/react'
import { Fragment } from 'react'

import useScroll from '@/hooks/useScroll'

const Header = ({ app }: { app?: boolean }) => {
  const router = useRouter()
  const { y } = useScroll()
  const isScrolled = y > 100

  const { data, status } = useSession()
  const isLoggedIn = status === 'authenticated'
  return (
    <header
      className={cx(
        'transition-smooth fixed z-50 w-full bg-white',
        [isScrolled ? 'py-3 shadow-lg' : 'py-3 lg:py-4'],
        [app && 'shadow-md']
      )}
    >
      <div className="container flex items-center justify-center sm:justify-between">
        <div className="inline-flex items-center">
          <span
            className={cx('transition-smooth text-blue-500', [
              isScrolled
                ? 'pl-2 text-lg tracking-[5px]'
                : 'pl-2 text-lg tracking-[5px] lg:pl-5 lg:text-[32px] lg:tracking-[10px]',
            ])}
          >
            Cinta Koding
          </span>
        </div>
        {!isLoggedIn ? (
          <>
            <Link href="/auth/signin">
              <a>
                <button
                  type="button"
                  className="text-white bg-gradient-to-br from-green-400 to-blue-600 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-green-200 dark:focus:ring-green-800 font-medium rounded-full text-sm px-5 py-2.5 text-center"
                >
                  Login
                </button>
              </a>
            </Link>
          </>
        ) : (
          <>
            <div className="w-56 text-right">
              <Menu as="div" className="relative inline-block text-left">
                <div>
                  <Menu.Button className="inline-flex w-full justify-center rounded-md bg-black bg-opacity-20 px-4 py-2 text-sm font-medium text-white hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75">
                    Welcome, {data?.name}
                  </Menu.Button>
                </div>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-100"
                  enterFrom="transform opacity-0 scale-95"
                  enterTo="transform opacity-100 scale-100"
                  leave="transition ease-in duration-75"
                  leaveFrom="transform opacity-100 scale-100"
                  leaveTo="transform opacity-0 scale-95"
                >
                  <Menu.Items className="absolute right-0 mt-2 w-56 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                    <div className="px-1 py-1 ">
                      <Menu.Item>
                        {({ active }) => (
                          <button
                            className={`${
                              active ? 'bg-blue-600 text-white' : 'text-gray-900'
                            } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                            onClick={() => router.push('/app/detail-profile')}
                          >
                            Detail Profile
                          </button>
                        )}
                      </Menu.Item>
                      <Menu.Item>
                        {({ active }) => (
                          <button
                            className={`${
                              active ? 'bg-blue-600 text-white' : 'text-gray-900'
                            } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                            onClick={() => signOut({ callbackUrl: '/' })}
                          >
                            Logout
                          </button>
                        )}
                      </Menu.Item>
                    </div>
                  </Menu.Items>
                </Transition>
              </Menu>
            </div>
          </>
        )}
      </div>
    </header>
  )
}

export default Header
