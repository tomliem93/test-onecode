import { Icon } from '@iconify/react'
import { useQuery } from '@tanstack/react-query'
import Link from 'next/link'

import api from '@/services/api'

type Props = {
  postId: number
  title: string
}
export const PostItemSkeleton = () => {
  return (
    <div className="border border-blue-300 mb-4 shadow rounded-md p-4 max-w-sm w-full mx-auto">
      <div className="animate-pulse flex space-x-4">
        <div className="h-2 bg-slate-200 rounded col-span-2"></div>
        <div className="flex-1 space-y-6 py-1">
          <div className="h-2 bg-slate-200 rounded"></div>
          <div className="space-y-3">
            <div className="grid grid-cols-3 gap-4">
              <div className="h-2 bg-slate-200 rounded col-span-2"></div>
              <div className="h-2 bg-slate-200 rounded col-span-1"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
export const PostItem = ({ postId, title }: Props) => {
  const { data } = useQuery(['PostComment', postId], () => api.master.getComments(postId))
  return (
    <div className="flex flex-row space-x-4 mb-4">
      <div className="font-semibold">UserName</div>
      <div className="flex flex-col">
        <p className="mb-1">{title}</p>
        <div className="flex flex-row space-x-4">
          <div className="mr-4 flex flex-row space-x-2 items-center">
            <Icon icon="bxs:comment-detail" /> <span>{data?.length || 0}</span>
          </div>
          <Link href={`/app/detail-post/${postId}`}>
            <a className="text-blue-600 font-semibold text-sm">Detail</a>
          </Link>
        </div>
      </div>
    </div>
  )
}
