import { Icon, IconifyIcon } from '@iconify/react'
import cx from 'clsx'
import { useEffect, useState } from 'react'

interface AlertProps {
  variant?: 'success' | 'danger' | 'warning' | 'info'
  floated?: boolean
  children?: React.ReactNode
}

function Alert({ variant = 'success', floated, children }: AlertProps) {
  const [showAlert, setShowAlert] = useState(true)
  const [dismissed, setDismissed] = useState(false)

  let icon: string | IconifyIcon
  icon = ''
  if (variant === 'success') {
    icon = 'akar-icons:circle-check'
  } else if (variant === 'danger') {
    icon = 'akar-icons:circle-x'
  } else if (variant === 'warning') {
    icon = 'akar-icons:circle-alert'
  } else if (variant === 'info') {
    icon = 'akar-icons:info'
  }

  const dismissAlert = () => {
    setDismissed(true)
    setTimeout(() => {
      setShowAlert(false)
    }, 300)
  }

  useEffect(() => {
    if (showAlert) {
      setTimeout(() => {
        dismissAlert()
      }, 2000)
    }
  }, [showAlert])

  return (
    <>
      {showAlert && (
        <div
          className={cx(
            'mb-4 inline-flex items-center justify-between break-all rounded-md py-4 pl-12 pr-14 text-sm',
            [floated ? 'fixed top-6 left-1/2 z-50 -translate-x-1/2 whitespace-nowrap shadow-2xl' : 'relative w-full'],
            [variant === 'success' && 'bg-emerald-100 text-emerald-600'],
            [variant === 'danger' && 'bg-rose-100 text-rose-600'],
            [variant === 'warning' && 'bg-yellow-100 text-yellow-600'],
            [variant === 'info' && 'bg-cyan-100 text-cyan-600'],
            [dismissed && 'opacity-0 transition-opacity duration-300 ease-out']
          )}
        >
          <Icon icon={icon} className="absolute left-4 mr-4 inline-block text-xl" />
          {children}
          <button
            type="button"
            className={cx(
              'absolute right-3 inline-flex items-center justify-center',
              'h-8 w-8 rounded',
              'transition-colors duration-200 ease-in-out',
              [variant === 'success' && 'hover:bg-emerald-200'],
              [variant === 'danger' && 'hover:bg-rose-200'],
              [variant === 'warning' && 'hover:bg-yellow-200'],
              [variant === 'info' && 'hover:bg-cyan-200']
            )}
            onClick={() => dismissAlert()}
          >
            <Icon
              icon="eva:close-fill"
              className={cx(
                'text-lg',
                [variant === 'success' && 'text-emerald-600'],
                [variant === 'danger' && 'text-danger-600'],
                [variant === 'warning' && 'text-yellow-600'],
                [variant === 'info' && 'text-cyan-600']
              )}
            />
          </button>
        </div>
      )}
    </>
  )
}

export default Alert
