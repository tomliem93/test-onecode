import { signOut } from 'next-auth/react'

export const checkError = (error: Error) => {
  console.log(error.message)
  if (error.message === 'Token Expired') {
    signOut({
      callbackUrl: '/auth/login?error=SessionExpired',
    })
  }
}
