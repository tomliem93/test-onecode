import axios from 'axios'
import { getSession } from 'next-auth/react'

const client = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL,
  headers: {
    'Content-type': 'application/json',
  },
})

client.interceptors.response.use((res) => {
  if (res.data.result === false && res.data.msg) {
    return Promise.reject(Error(res.data.msg))
  }

  return res
})

const clientPrivate = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL,
  headers: {
    'Content-type': 'application/json',
  },
})

clientPrivate.interceptors.request.use(async (req) => {
  const session = await getSession()
  if (session && req.headers) {
    req.headers['x-api-key'] = `Bearer ${session.accessToken}`
  }
  return req
})

clientPrivate.interceptors.response.use((res) => {
  if (res.data.result === false && res.data.msg) {
    return Promise.reject(Error(res.data.msg))
  }
  return res
})

export { client, clientPrivate }
