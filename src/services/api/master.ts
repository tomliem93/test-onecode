import { clientPrivate } from './axios'

export interface PostsData {
  userId: number
  id: number
  title: string
  body: string
}

export interface CommentsData {
  postId: number
  id: number
  name: string
  email: string
  body: string
}

const getPosts = async ({
  search,
  page,
  limit,
}: {
  search: string
  page: number
  limit: number
}): Promise<{ data: PostsData[]; total: number }> => {
  const response = await clientPrivate.get('/posts')
  const pagedData = response.data.slice(page * limit, page * limit + limit)
  if (!search) return { data: pagedData, total: response?.data?.length || 0 }

  const filteredData = pagedData.filter((item: PostsData) => item.title.includes(search) || item.body.includes(search))
  return { data: filteredData, total: filteredData?.length || 0 }
}
const getPostDetail = async (postId: number): Promise<PostsData> => {
  const response = await clientPrivate.get(`/posts/${postId}`)

  return response.data
}
const getComments = async (postId: number): Promise<CommentsData[]> => {
  const response = await clientPrivate.get(`/posts/${postId}/comments`)

  return response.data
}

export default { getPosts, getComments, getPostDetail }
