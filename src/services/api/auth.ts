import { client } from './axios'

export interface LoginReqData {
  username: string
  password: string
}

export interface LoginResData {
  id: number
  name: string
  username: string
  email: string

  address: {
    street: string
    suite: string
    city: string
    zipcode: string
  }
  phone: string
}

const submitLogin = async (data: LoginReqData): Promise<LoginResData | null> => {
  const response = await client.get<Array<LoginResData>>('/users')

  if (!response.data) return null

  const user = response.data.find((item) => item.username === data.username && data.password === item.username)
  if (!user) return null
  return user
}

export default { submitLogin }
