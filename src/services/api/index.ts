import auth from './auth'
import master from './master'

export default { auth, master }
