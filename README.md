## Available Scripts

| Script       | Description       |
| ------------ | ----------------- |
| `yarn dev`   | Start development |
| `yarn build` | Build app         |
| `yarn start` | Start build app   |
| `yarn lint`  | Code linter       |
